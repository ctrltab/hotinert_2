
import 'package:flame/game.dart';
import 'package:flutter/material.dart';
import 'dart:async';
import 'package:esense_flutter/esense.dart';
import 'package:hotinert_two/earable_gesture_detector.dart';


import 'game.dart';
import 'maze.dart';

void main() => runApp(MaterialApp(home: MyApp()));

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  String _deviceName = 'Unknown';
  double _voltage = -1;
  String _deviceStatus = '';
  bool sampling = false;
  String _event = '';
  String _button = 'not pressed';
  bool connected = false;

  double offsetX = 0;
  double offsetY = 0;
  double offsetZ = 0;

  double calibrateX = 0;
  double calibrateY = 0;
  double calibrateZ = 0;

  bool calibrate = false;
  List<List<double>> calibrateList = List.empty(growable: true);

  EarableGestureDetector detector = EarableGestureDetector();

  MyGame? game;

  // the name of the eSense device to connect to -- change this to your own device.
  String eSenseName = 'eSense-0320';

  void initState() {
    super.initState();
    _listenToESense();
  }

  Future _listenToESense() async {
    // if you want to get the connection events when connecting,
    // set up the listener BEFORE connecting...
    ESenseManager().connectionEvents.listen((event) {
      print('CONNECTION event: $event');

      // when we're connected to the eSense device, we can start listening to events from it
      if (event.type == ConnectionType.connected) _listenToESenseEvents();

      setState(() {
        connected = false;
        switch (event.type) {
          case ConnectionType.connected:
            _deviceStatus = 'connected';
            connected = true;
            break;
          case ConnectionType.unknown:
            _deviceStatus = 'unknown';
            break;
          case ConnectionType.disconnected:
            _deviceStatus = 'disconnected';
            break;
          case ConnectionType.device_found:
            _deviceStatus = 'device_found';
            break;
          case ConnectionType.device_not_found:
            _deviceStatus = 'device_not_found';
            break;
        }
      });
    });
  }

  Future _connectToESense() async {
    print('connecting... connected: $connected');
    if (!connected) connected = await ESenseManager().connect(eSenseName);

    setState(() {
      _deviceStatus = connected ? 'connecting' : 'connection failed';
    });
  }

  Future _calibrate() async {
    calibrate = !calibrate;
    if (!calibrate) {
      calibrateList.forEach((element) {
        calibrateX += element[0];
        calibrateY += element[1];
        calibrateZ += element[2];
      });

      setState(() {
        calibrateX = calibrateX / calibrateList.length;
        calibrateY = calibrateY / calibrateList.length;
        calibrateZ = calibrateZ / calibrateList.length;
      });
    }
  }

  void _listenToESenseEvents() async {
    ESenseManager().eSenseEvents.listen((event) {
      setState(() {
        switch (event.runtimeType) {
          case DeviceNameRead:
            _deviceName = (event as DeviceNameRead).deviceName;
            break;
          case BatteryRead:
            _voltage = (event as BatteryRead).voltage;
            break;
          case ButtonEventChanged:
            _button = (event as ButtonEventChanged).pressed
                ? 'pressed'
                : 'not pressed';
            break;
          case AccelerometerOffsetRead:
            var eventa = event as AccelerometerOffsetRead;
            offsetX = eventa.offsetX / 2048;
            offsetY = eventa.offsetY / 2048;
            offsetZ = eventa.offsetZ / 2048;
            break;
          case AdvertisementAndConnectionIntervalRead:
          // TODO
            break;
          case SensorConfigRead:
          // TODO
            break;
        }
      });
    });

    _getESenseProperties();
  }

  void _getESenseProperties() async {
    // get the battery level every 10 secs
    Timer.periodic(
      Duration(seconds: 10),
          (timer) async =>
      (connected) ? await ESenseManager().getBatteryVoltage() : null,
    );

    // wait 2, 3, 4, 5, ... secs before getting the name, offset, etc.
    // it seems like the eSense BTLE interface does NOT like to get called
    // several times in a row -- hence, delays are added in the following calls
    Timer(Duration(seconds: 2),
            () async => await ESenseManager().getDeviceName());
    Timer(Duration(seconds: 3),
            () async => await ESenseManager().getAccelerometerOffset());
    Timer(
        Duration(seconds: 4),
            () async =>
        await ESenseManager().getAdvertisementAndConnectionInterval());
    Timer(Duration(seconds: 5),
            () async => await ESenseManager().getSensorConfig());
  }

  StreamSubscription? subscription;

  void _startListenToSensorEvents() async {
    // subscribe to sensor event from the eSense device
    DateTime time = DateTime.now();
    subscription = ESenseManager().sensorEvents.listen((event) {
      double delta = event.timestamp.difference(time).inMicroseconds / 1000000.0;
      time = event.timestamp;
      //print('SENSOR event: $event');
      setState(() {
        _event = event.toString();

        if (event.runtimeType == SensorEvent) {
          var eventa = event as SensorEvent;
          var accel = eventa.gyro;
          if (calibrate)
            calibrateList.add(
                List.of([accel[0] / 65.5, accel[1] / 65.5, accel[2] / 65.5]));

          //game?.movement(accel[0] / 65.5 - calibrateX, accel[1] / 65.5 -
          //    calibrateY, accel[2] / 65.5 - calibrateZ);

          detector.movement(accel[0] / 65.5 - calibrateX, accel[1] / 65.5 -
              calibrateY, accel[2] / 65.5 - calibrateZ, delta);
        }
      });
    });
    setState(() {
      sampling = true;
    });
  }

  void _pauseListenToSensorEvents() async {
    subscription!.cancel();
    this.detector.unregisterListener(game!);
    setState(() {
      sampling = false;
    });
  }

  void dispose() {
    _pauseListenToSensorEvents();
    ESenseManager().disconnect();
    super.dispose();
  }

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('eSense Demo App'),
      ),
      body: Align(
        alignment: Alignment.topLeft,
        child: ListView(
          children: [
            Text('eSense Device Status: \t$_deviceStatus'),
            Text('eSense Device Name: \t$_deviceName'),
            Text('eSense Battery Level: \t$_voltage'),
            Text('eSense Button Event: \t$_button'),
            Text(''),
            Text('$_event'),
            Container(
              height: 140,
              width: 200,
              decoration:
              BoxDecoration(borderRadius: BorderRadius.circular(10)),
              child: ListView(
                children: [
                  TextButton.icon(
                    onPressed: _connectToESense,
                    icon: Icon(Icons.login),
                    label: Text(
                      'CONNECT....',
                      style: TextStyle(fontSize: 35),
                    ),
                  ),
                  TextButton.icon(
                    onPressed: _calibrate,
                    icon: Icon(Icons.accessibility),
                    label: Text(
                      'calibrate',
                      style: TextStyle(fontSize: 35),
                    ),
                  ),
                  TextButton(
                    child: Text('Start Game'),
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) {
                          game = MyGame(Maze.prim(12, 16, Point(0, 2)));
                          detector.registerListener(game!);
                          return GameWidget(game: game!);
                        }),
                      );
                    },
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        // a floating button that starts/stops listening to sensor events.
        // is disabled until we're connected to the device.
        onPressed: (!ESenseManager().connected)
            ? null
            : (!sampling)
            ? _startListenToSensorEvents
            : _pauseListenToSensorEvents,
        tooltip: 'Listen to eSense sensors',
        child: (!sampling) ? Icon(Icons.play_arrow) : Icon(Icons.pause),
      ),
    );
  }
}
