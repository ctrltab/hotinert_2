enum EarableMovement { UP, DOWN, LEFT, RIGHT }

class EarableGestureDetector {
  double threshold = 10;
  double time = 0;
  List<EarableGestureListener> listeners = List.empty(growable: true);
  double lastX = 0;
  double lastZ = 0;

  EarableGestureDetector();

  void registerListener(EarableGestureListener listener) {
    if (!listeners.contains(listener)) listeners.add(listener);
  }

  void unregisterListener(EarableGestureListener listener) {
    listeners.remove(listener);
  }

  void movement(double x, double y, double z, double t) {
    time += t;
    if (x < -threshold && x > lastX) {
      handleMovement(EarableMovement.LEFT);
    } else if (x > threshold && x < lastX) {
      handleMovement(EarableMovement.RIGHT);
    }

    if (z > threshold && z > lastZ) {
      handleMovement(EarableMovement.DOWN);
    } else if (z < -threshold && z < lastZ) {
      handleMovement(EarableMovement.UP);
    }

    if (x < -threshold || x > threshold) {
      lastX = x;
    } else {
      lastX = 0;
    }
    if (z < -threshold || z > threshold) {
      lastZ = z;
    } else {
      lastZ = 0;
    }
  }

  void handleMovement(EarableMovement direction) {
    listeners.forEach((element) {
      element.gesture(direction, time);
    });
    time = 0;
  }
}

abstract class EarableGestureListener {
  void gesture(EarableMovement direction, double delta);
}
