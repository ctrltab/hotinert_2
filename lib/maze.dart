import 'dart:core';
import 'dart:math';

import 'package:equatable/equatable.dart';

class Maze {
  int width;
  int height;

  Point start;
  Point end;

  List<Wall> walls;

  Maze(this.width, this.height, this.start, this.end, this.walls);

  factory Maze.prim(int width, int height, Point start) {
    Random rnd = Random();
    Point end;
    do{
      end = Point(rnd.nextInt(width), rnd.nextInt(height));
    } while(pow(end.x - start.x, 2) + pow(end.y - start.y, 2) < 25);

    List<Wall> walls = List.empty(growable: true);
    Set<Wall> walls2 = Set.of([]);

    Set<Point> visited = Set.of([]);

    for (int x = 0; x < width; x++) {
      for (int y = 0; y < height; y++) {
        if (x - 1 >= 0) {
          walls.add(Wall(Point(x, y), Point(x - 1, y)));
        }
        if (y - 1 >= 0) {
          walls.add(Wall(Point(x, y), Point(x, y - 1)));
        }
      }
    }

    visited.add(start);

    int x = start.x;
    int y = start.y;

    if (x - 1 >= 0) {
      walls2.add(Wall(Point(x, y), Point(x - 1, y)));
    }
    if (y - 1 >= 0) {
      walls2.add(Wall(Point(x, y), Point(x, y - 1)));
    }
    if (x + 1 < width) {
      walls2.add(Wall(Point(x + 1, y), Point(x, y)));
    }
    if (y + 1 < height) {
      walls2.add(Wall(Point(x, y  + 1), Point(x, y)));
    }

    while (walls2.isNotEmpty) {
      var wall = walls2.elementAt(rnd.nextInt(walls2.length));
      Point fir = wall.fir;
      Point sec = wall.sec;

      if (visited.contains(fir) && !visited.contains(sec) ||
          visited.contains(sec) && !visited.contains(fir)) {
        print(walls.remove(wall));

        Point vis = fir;
        if (visited.contains(fir)) {
          vis = sec;
        }
        visited.add(vis);

        int x = vis.x;
        int y = vis.y;

        if (x - 1 >= 0) {
          walls2.add(Wall(Point(x, y), Point(x - 1, y)));
        }
        if (y - 1 >= 0) {
          walls2.add(Wall(Point(x, y), Point(x, y - 1)));
        }
        if (x + 1 < width) {
          walls2.add(Wall(Point(x + 1, y), Point(x, y)));
        }
        if (y + 1 < height) {
          walls2.add(Wall(Point(x, y + 1), Point(x, y)));
        }
      }

      walls2.remove(wall);
    }

    return Maze(width, height, start, end, walls);
  }

  bool hasWall(int x1, int y1, int x2, int y2) {
    return walls.contains(Wall(Point(x1, y1), Point(x2, y2))) ||
        walls.contains(Wall(Point(x2, y2), Point(x1, y1)));
  }
}

class Point extends Equatable {
  final int x;
  final int y;

  Point(this.x, this.y);

  @override
  List<Object?> get props => [x, y];

  @override
  bool get stringify => true;
}

class Wall extends Equatable {
  final Point fir;
  final Point sec;

  Wall(this.fir, this.sec);

  @override
  List<Object?> get props => [fir, sec];

  @override
  bool get stringify => true;
}
