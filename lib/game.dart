import 'dart:ui';

import 'package:flame/components.dart';
import 'package:flame/flame.dart';
import 'package:flame/gestures.dart';
import 'package:flame/game.dart';
import 'package:flame/palette.dart';
import 'package:hotinert_two/earable_gesture_detector.dart';
import 'dart:math' as math;

import 'maze.dart';

class Palette {
  static const PaletteEntry white = BasicPalette.white;
  static const PaletteEntry background = PaletteEntry(Color(0x90009000));
  static const PaletteEntry player = PaletteEntry(Color(0xF0F09000));
  static const PaletteEntry red = PaletteEntry(Color(0xFFFF0000));
  static const PaletteEntry blue = PaletteEntry(Color(0xFF0000FF));
}

class StartTile extends PositionComponent with HasGameRef<MyGame> {
  @override
  void render(Canvas c) {
    super.render(c);
    c.drawRect(Rect.fromLTWH(0, 0, width, height), Palette.blue.paint());
  }

  @override
  void onMount() {
    super.onMount();
    width = height = gameRef.squareSize;
    anchor = Anchor.center;
  }
}

class EndTile extends PositionComponent with HasGameRef<MyGame> {
  @override
  void render(Canvas c) {
    super.render(c);
    c.drawRect(Rect.fromLTWH(0, 0, width, height), Palette.red.paint());
  }

  @override
  void onMount() {
    super.onMount();
    width = height = gameRef.squareSize;
    anchor = Anchor.center;
  }
}

class Border extends PositionComponent with HasGameRef<MyGame> {
  @override
  void render(Canvas c) {
    super.render(c);
    c.drawRect(Rect.fromLTWH(-6, -6, width, height), Palette.white.paint());
    c.drawRect(Rect.fromLTWH(0, 0, width - 12, height - 12),
        Palette.background.paint());
  }

  @override
  void onMount() {
    super.onMount();
    width = gameRef.level.maze.width * gameRef.squareSize + 12;
    height = gameRef.level.maze.height * gameRef.squareSize + 12;
    anchor = Anchor.topLeft;

    x = -gameRef.squareSize / 2;
    y = -gameRef.squareSize / 2;
  }
}

class Player extends PositionComponent with HasGameRef<MyGame> {
  @override
  void render(Canvas c) {
    super.render(c);
    c.drawRect(Rect.fromLTWH(0, 0, width, height), Palette.player.paint());
  }

  @override
  void onMount() {
    super.onMount();
    width = gameRef.squareSize;
    height = gameRef.squareSize;
    anchor = Anchor.center;

    x = gameRef.level.maze.start.x * gameRef.squareSize;
    y = gameRef.level.maze.start.y * gameRef.squareSize;
  }
}

class Wall extends PositionComponent with HasGameRef<MyGame> {
  Point start;
  Point end;

  Wall(this.start, this.end);

  @override
  void render(Canvas c) {
    super.render(c);

    c.drawRect(Rect.fromLTWH(0, 0, width, height), Palette.white.paint());
  }

  @override
  void onMount() {
    super.onMount();
    width = gameRef.squareSize;
    height = 4;
    anchor = Anchor.bottomLeft;

    if (start.x == end.x) {
      x = (start.x - 0.5) * gameRef.squareSize;
      y = ((start.y - 0.5)) * gameRef.squareSize + height / 2;
    } else {
      x = ((start.x - 0.5)) * gameRef.squareSize - height / 2;
      y = (start.y - 0.5) * gameRef.squareSize;
      angle = math.pi / 2;
    }
  }
}

class Level extends PositionComponent with HasGameRef<MyGame> {
  Maze maze;

  Level(this.maze);

  @override
  void render(Canvas canvas) {
    super.render(canvas);
  }

  @override
  void onMount() {
    super.onMount();

    addChild(Border());

    this.maze.walls.forEach((element) {
      addChild(Wall(element.fir, element.sec));
    });

    addChild(StartTile()
      ..x = this.maze.start.x.toDouble() * gameRef.squareSize
      ..y = this.maze.start.y.toDouble() * gameRef.squareSize);

    addChild(EndTile()
      ..x = this.maze.end.x.toDouble() * gameRef.squareSize
      ..y = this.maze.end.y.toDouble() * gameRef.squareSize);
  }

  @override
  void onGameResize(Vector2 canvasSize) {
    super.onGameResize(canvasSize);

    if (canvasSize.x > canvasSize.y) {
      gameRef.camera.zoom = 0.75;
    } else {
      gameRef.camera.zoom = 1;
    }

    gameRef.camera.snapTo(Vector2(
        (this.maze.width / 2) * gameRef.squareSize - 12 - canvasSize.x / (2 * gameRef.camera.zoom),
        (this.maze.height / 2) * gameRef.squareSize - 12 - canvasSize.y / (2 * gameRef.camera.zoom)));
  }
}

enum Movement { UP, DOWN, LEFT, RIGHT, NONE }

class MyGame extends BaseGame
    with DoubleTapDetector, TapDetector
    implements EarableGestureListener {
  final double squareSize = 25;
  bool running = true;

  Maze initialMaze;
  late Level level;
  late Player player;

  int score = 0;
  late TextComponent text;

  bool moveable = true;

  MyGame(this.initialMaze);

  @override
  void onAttach() {
    level = Level(initialMaze);
    add(level);
    player = Player();
    add(player);
    text = TextComponent("Score: $score");
    add(text);
    super.onAttach();
  }

  @override
  void onTapDown(TapDownInfo event) {
    super.onTapDown(event);
    moveable = false;
  }

  @override
  void onTapUp(TapUpInfo event) {
    super.onTapUp(event);
    moveable = true;
  }

  @override
  void onDoubleTap() {
    if (running) {
      pauseEngine();
    } else {
      resumeEngine();
    }

    running = !running;
  }

  void movePlayer(Movement direction) {
    if (!moveable) return;
    int oldX = player.x ~/ squareSize;
    int oldY = player.y ~/ squareSize;

    int newX = oldX;
    int newY = oldY;
    switch (direction) {
      case Movement.LEFT:
        newX = (oldX - 1);
        break;
      case Movement.RIGHT:
        newX = (oldX + 1);
        break;
      case Movement.UP:
        newY = (oldY - 1);
        break;
      case Movement.DOWN:
        newY = (oldY + 1);
        break;
      case Movement.NONE:
        break;
    }

    if (this.level.maze.hasWall(oldX, oldY, newX, newY)) {
      return;
    }

    if (newX < 0 ||
        newX >= this.level.maze.width ||
        newY < 0 ||
        newY >= this.level.maze.height) {
      return;
    }

    if (newX == this.level.maze.end.x && newY == this.level.maze.end.y) {
      remove(level);
      level = Level(
          Maze.prim(level.maze.width, level.maze.height, Point(newX, newY)));
      add(level);
      changePriority(player, 99);
      score++;
      text.text = "Score: $score";
      changePriority(text, 100);
    }

    player.x = newX * squareSize;
    player.y = newY * squareSize;
  }

  @override
  void gesture(EarableMovement direction, double delta) {
    print("$direction . $delta");
    Movement? move;
    if (delta > 0.01) {
      switch (direction) {
        case EarableMovement.LEFT:
          move = Movement.LEFT;
          break;
        case EarableMovement.DOWN:
          move = Movement.DOWN;
          break;
        case EarableMovement.UP:
          move = Movement.UP;
          break;
        case EarableMovement.RIGHT:
          move = Movement.RIGHT;
          break;
      }
      movePlayer(move);
    }
  }
}
